## XMake

### 项目介绍

**XMake** 是根据 ```ThinkPHP``` 的自定义指令创建 ```ThinkAdmin``` 风格的控制器、模型、服务和模板文件。

>*本人比较菜* 生成的代码比较简单，**并没有关联数据表字段**，代码生成后还是需要人工**手动调整**，代码写的不是很好，不喜勿喷，喜欢的请点个 ```Star``` 谢谢~

### 安装方法

* 项目使用了TP的工具库，请提前安装好工具库 ```composer require topthink/think-helper```
* 复制 ```src``` 里面的 ```xmake``` 文件夹到 ```app/command``` 目录下（如果 ```command``` 文件夹不存在请创建）
* 复制 ```src``` 里面的 ```console.php``` 文件到 ```app/config``` 目录下
* 如果项目本身已存在 ```console.php``` 文件，请根据 ```src/console.php``` 里面的commands手动合并到自己项目中

### 使用方法

```
### 在项目根目录直接执行 
php think xmake 表名称

### 例如数据库表为：shop_goods
php think xmake shop_goods --title 商城商品管理
``` 

* 第一个 ```_``` 前面的 ```shop``` 是应用名称

**生成的结构样例：**

![生成的结构样例](https://gitee.com/baima/xmake/raw/master/img/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20230707165148.png)

**还可以指定名称**

```
php think xmake user --app shop --controller User --model ShopUser --title 商城用户管理
``` 